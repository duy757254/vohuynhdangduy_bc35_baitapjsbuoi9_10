function getInforFromForm() {
  var account = document.getElementById("tknv").value;
  var name = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var password = document.getElementById("password").value;
  var dayWork = document.getElementById("datepicker").value;
  var basicSalary = document.getElementById("luongCB").value ;
  var position = document.getElementById("chucvu").value;
  var hourWork = document.getElementById("gioLam").value

  var Nv = new nhanVien(
    account,
    name,
    email,
    password,
    dayWork,
    basicSalary,
    position,
    hourWork
  );

  return Nv;
}

function renderListNv(list) {
  var contentHTML = "";
  for (var i = 0; i < list.length; i++) {
    var contentNv = list[i];
    var contentTr = `<tr>
        <td>${contentNv.account}</td>
        <td>${contentNv.name}</td>
        <td>${contentNv.email}</td>
        <td>${contentNv.dayWork}</td>
        <td>${contentNv.posn}</td>
        <td>${contentNv.totalSalary()}</td>
        <td class="list-group" >${contentNv.rating()}</td>
        <td>
        <button data-target="#myModal" data-toggle="modal" onclick="editNv('${contentNv.account}')" class="btn btn-primary m-2" >Sửa</button>
        <button  onclick="deleteNv('${contentNv.account}')" class="btn btn-danger m-2" >Xóa</button>
        </td>
        </tr>
        `;
        contentHTML = contentHTML + contentTr;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

function showInforUpToForm(NV){
    document.getElementById("tknv").value=NV.account
    document.getElementById("name").value=NV.name
    document.getElementById("email").value=NV.email
    document.getElementById("password").value=NV.password
    document.getElementById("datepicker").value=NV.dayWork
    document.getElementById("luongCB").value=NV.basic
    document.getElementById("chucvu").value=NV.posn
    document.getElementById("gioLam").value=NV.hour
    
}
function resetForm(){
    document.getElementById("formQLNV").reset()
}