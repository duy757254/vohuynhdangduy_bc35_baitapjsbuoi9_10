var dsNV = [];

var dataJSON = localStorage.getItem("ListNV");

if (dataJSON) {
  var dataRaw = JSON.parse(dataJSON);
  dsNV = dataRaw.map(function (item) {
    return new nhanVien(
      item.account,
      item.name,
      item.email,
      item.password,
      item.dayWork,
      item.basic,
      item.posn,
      item.hour
    );
  });
  renderListNv(dsNV);
}

var reset1 = () => {
  document.getElementById("tknv").disabled = false;
  resetForm()
  // khi bấm nút thêm nhân viên nút cập nhật sẻ ẩn
  document.getElementById("btnCapNhat").style.display = "none";
};

// tạo save localstorage
function saveLocalStorage() {
  listNVJSON = JSON.stringify(dsNV);
  localStorage.setItem("ListNV", listNVJSON);
  renderListNv(dsNV);
}


// Thêm nhân viên
function addNv() {
  var newNv = getInforFromForm();
  var isValid = true;
  // check tài khoảng
  isValid =
    isValid & checkEmty(newNv.account, "tbTKNV") &&
    checkLimit(newNv.account, "tbTKNV")&&checkAccount(newNv.account,dsNV,"tbTKNV");
  // Check họ và tên
  isValid &= checkEmty(newNv.name, "tbTen") && checkName(newNv.name, "tbTen");
  // check email
  isValid &=
    checkEmty(newNv.email, "tbEmail") && checkEmail(newNv.email, "tbEmail");
  // check Mật khẩu
  isValid &=
    checkEmty(newNv.password, "tbMatKhau") &&
    checkPassword(newNv.password, "tbMatKhau");
  // check ngày làm việc
  isValid &= checkEmty(newNv.dayWork, "tbNgay");
  // check lương cơ bản
  isValid &=
    checkEmty(newNv.basic, "tbLuongCB") &&
    checkSalary(newNv.basic, "tbLuongCB");
  // check chức vụ
  isValid &= checkPosition(newNv.posn, "tbChucVu");
  // check giờ làm
  isValid &=
    checkEmty(newNv.hour, "tbGiolam") && checkTimeWork(newNv.hour, "tbGiolam");

  if (isValid) {
    dsNV.push(newNv);
    saveLocalStorage();
    renderListNv(dsNV);
    resetForm()
  }
}
// Chỉnh sửa thông tin nhân viên
function editNv(acc) {
  var index = dsNV.findIndex(function (Nv) {
    return Nv.account == acc;
  });
  if (index == -1) {
    return;
  }
  var Nv = dsNV[index];

  showInforUpToForm(Nv);

  document.getElementById("tknv").disabled = true;
  // khi bấm nút sửa nút cập nhật sẻ trở lại ban đầu
  document.getElementById("btnCapNhat").style.display = "block";
}
// Xóa Thông tin nhân viên
function deleteNv(acc) {
  var index = dsNV.findIndex(function (Nv) {
    return Nv.account == acc;
  });
  if (index == -1) {
    return;
  }
  dsNV.splice(index, 1);

  saveLocalStorage();
  renderListNv(dsNV);
}

dsNV.forEach(function (item) {});

//cập nhật thông tin nhân viên
function updateInfor() {
  var edit = getInforFromForm();
  var index = dsNV.findIndex(function (Nv) {
    return Nv.account == edit.account;
  });
  if (index == -1) return;
  dsNV[index] = edit;
  saveLocalStorage();
  renderListNv(dsNV);
}

