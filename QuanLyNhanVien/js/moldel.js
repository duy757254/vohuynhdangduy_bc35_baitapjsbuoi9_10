function nhanVien(
  _account,
  _name,
  _email,
  _password,
  _dayWork,
  _basicSalary,
  _position,
  _hourWork
) {
  this.account = _account;
  this.name = _name;
  this.email = _email;
  this.password = _password;
  this.dayWork = _dayWork;
  this.basic = _basicSalary;
  this.posn = _position;
  this.hour = _hourWork;
  this.totalSalary = function () {
    if (this.posn == "Giám Đốc") {
      return (this.basic*1) * 3;
    } else if (this.posn == "Trưởng Phòng") {
      return (this.basic*1) * 2;
    } else if (this.posn == "Nhân Viên") {
      return (this.basic*1);
    }
  };
  this.rating=function(){
    
    if (this.hour>=192){
        return  "Xuất Sắc"
    }else if(this.hour>=176&&this.hour<192){
        return "Giỏi"

    }else if (this.hour>=160&&this.hour<176){
        return "Khá"

    }else{
        return "Trung Bình"
    }
}
}

