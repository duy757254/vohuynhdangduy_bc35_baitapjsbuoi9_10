function checkEmty(value, idErr) {
  if (value.length == 0) {
    document.getElementById(idErr).innerText = "Ô này không được để trống";
    return false;
  } else {
    document.getElementById(idErr).innerText = "";
    return true;
  }
}
function checkPosition(value, idErr) {
  if (value == 0) {
    document.getElementById(idErr).innerText = "Hãy chọn vị trí nhân viên";
    return false;
  } else {
    document.getElementById(idErr).innerText = "";
    return true;
  }
}
function checkEmail(value, idErr) {
  const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  var isEmail = re.test(value);
  if (!isEmail) {
    document.getElementById(idErr).innerText = "Email này không hợp lệ.";
    return false;
  } else {
    document.getElementById(idErr).innerText = "";
    return true;
  }
}

function checkLimit(value, idErr) {
  if (value.length < 4 || value.length > 6) {
    document.getElementById(idErr).innerText =
      "Tài khoảng phải tối đa 6 ký tự, lớn hơn 4 ký tự";
    return false;
  } else {
    document.getElementById(idErr).innerText = "";
    return true;
  }
}

function checkName(value, idErr) {
  const re = /^(?! *$)[a-zA-Z.+ '-]+$/;
  var isName = re.test(value);
  if (!isName) {
    document.getElementById(idErr).innerText =
      "Tên phải là chữ cái và không dấu";
    return false;
  } else {
    document.getElementById(idErr).innerText = "";
    return true;
  }
}

function checkPassword(value, idErr) {
  const re =
    /(?=^.{6,10}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/;
  var isPassword = re.test(value);
  if (!isPassword) {
    document.getElementById(idErr).innerText =
      "Mật khẩu phải chứa từ 6-10 ký tự bao gồm ít nhất 1 chữ cái viết hoa, một số, một ký tự đặc biệt";
    return false;
  } else {
    document.getElementById(idErr).innerText = "";
    return true;
  }
}
function checkSalary(value, idErr) {
  if (value < 1000000 || value > 20000000) {
    document.getElementById(idErr).innerText =
      "Số không hợp lệ.Lương phải nằm trong khoảng từ 1000000 đến 20000000";
    return false;
  } else {
    document.getElementById(idErr).innerText = "";
    return true;
  }
}
function checkTimeWork(value,idErr){
  if (value<80 || value >200){
    document.getElementById(idErr).innerText="Số giờ làm chỉ được nằm trong khoảng 80-200 giờ"
    return false;

  }else {
    document.getElementById(idErr).innerText=""
    return true
  }
}
function checkAccount(id,list,iderr){
var index=list.findIndex(function(NV){
  return NV.account == id
})
if(index==-1){
  document.getElementById(iderr).innerText=""
  return true
}else{
  document.getElementById(iderr).innerText="Tài Khoảng nhân viên đã tồn tại"
  return false
}
}